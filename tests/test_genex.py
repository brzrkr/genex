import unittest
from itertools import product
from arpeggio import ParserPython, NoMatch
from exceptions import (
    GroupNotFoundException,
    NonsensicalQuantifierException,
)
from genex import PARSER_CONFIG, generate
from rules import genex_root, CONSONANTS, SYMBOLS, VOWELS, WORD_CHARS


if __name__ == "__main__":
    unittest.main()


class BaseTest(unittest.TestCase):
    parser = None

    def setUp(self):
        self.parser = ParserPython(genex_root, **PARSER_CONFIG, debug=False)


class TestLiteral(BaseTest):
    def test_single_char(self):
        results = generate("a", parser=self.parser)

        self.assertSetEqual(results, {"a"})

    def test_multiple_chars(self):
        results = generate("abcd", parser=self.parser)

        self.assertSetEqual(results, {"abcd"})

    def test_repetition(self):
        results = generate("a{2}", parser=self.parser)

        self.assertSetEqual(results, {"aa"})

    def test_minmax(self):
        results = generate("a{1,4}", parser=self.parser)

        self.assertSetEqual(results, {"a", "aa", "aaa", "aaaa"})


class TestPositiveCharClass(BaseTest):
    def test_single_char(self):
        results = generate("[a]", parser=self.parser)

        self.assertSetEqual(results, {"a"})

    def test_multiple_chars(self):
        results = generate("[abcd]", parser=self.parser)

        self.assertSetEqual(results, set("abcd"))

    def test_meta_seq(self):
        results = generate(r"[\vq]", parser=self.parser)

        self.assertSetEqual(results, VOWELS | {"q"})

    def test_repetition(self):
        results = generate("[abcd]{2}", parser=self.parser)
        expected = set(c * 2 for c in "abcd")

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate("[abcd]{1,2}", parser=self.parser)
        expected = set(c * 2 for c in "abcd") | set("abcd")

        self.assertSetEqual(results, expected)

    def test_pattern_repetition(self):
        results = generate("[abcd]!{2}", parser=self.parser)
        expected = set("".join(c) for c in product(set("abcd"), repeat=2))

        self.assertSetEqual(results, expected)

    def test_pattern_minmax(self):
        results = generate("[abcd]!{2,3}", parser=self.parser)
        expected = set("".join(c) for c in product(set("abcd"), repeat=2)) | set(
            "".join(c) for c in product(set("abcd"), repeat=3)
        )

        self.assertSetEqual(results, expected)


class TestNegativeCharClass(BaseTest):
    def test_single_char(self):
        results = generate("[^a]", parser=self.parser)
        expected = WORD_CHARS - {"a"}

        self.assertSetEqual(results, expected)

    def test_multiple_chars(self):
        results = generate("[^abcd]", parser=self.parser)
        expected = WORD_CHARS - set("abcd")

        self.assertSetEqual(results, expected)

    def test_meta_seq(self):
        results = generate(r"[^\vq]", parser=self.parser)
        expected = WORD_CHARS - VOWELS - {"q"}

        self.assertSetEqual(results, expected)

    def test_repetition(self):
        results = generate("[^abcd]{2}", parser=self.parser)
        expected = set(c * 2 for c in (WORD_CHARS - set("abcd")))

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate("[^abcd]{1,2}", parser=self.parser)
        chars = WORD_CHARS - set("abcd")
        expected = set(c * 2 for c in chars) | chars

        self.assertSetEqual(results, expected)

    def test_pattern_repetition(self):
        results = generate("[^abcd]!{2}", parser=self.parser)
        expected = set("".join(c) for c in product(WORD_CHARS - set("abcd"), repeat=2))

        self.assertSetEqual(results, expected)

    def test_pattern_minmax(self):
        results = generate("[^abcd]!{2,3}", parser=self.parser)
        expected = set(
            "".join(c) for c in product(WORD_CHARS - set("abcd"), repeat=2)
        ) | set("".join(c) for c in product(WORD_CHARS - set("abcd"), repeat=3))

        self.assertSetEqual(results, expected)


class TestMetaSequence(BaseTest):
    def test_simple(self):
        results = generate(r"\c", parser=self.parser)
        expected = CONSONANTS

        self.assertSetEqual(results, expected)

    def test_repetition(self):
        results = generate(r"\c{3}", parser=self.parser)
        expected = set(c * 3 for c in CONSONANTS)

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate(r"\c{0,2}", parser=self.parser)
        expected = set(s * quantity for s in CONSONANTS for quantity in range(0, 3)) - {
            ""
        }

        self.assertSetEqual(results, expected)

    def test_pattern_repetition(self):
        results = generate(r"\s!{3}", parser=self.parser)
        expected = set("".join(x) for x in product(SYMBOLS, repeat=3))

        self.assertSetEqual(results, expected)

    def test_pattern_minmax(self):
        results = generate(r"\s!{2,3}", parser=self.parser)
        expected = set("".join(x) for x in product(SYMBOLS, repeat=2)) | set(
            "".join(x) for x in product(SYMBOLS, repeat=3)
        )

        self.assertSetEqual(results, expected)


class TestCompositeExpression(BaseTest):
    def test_literal_class_meta(self):
        results = generate(r"a[ab]\s", parser=self.parser)
        expected = set("a" + c + s for c in "ab" for s in SYMBOLS)

        self.assertSetEqual(results, expected)

    def test_repetition(self):
        results = generate(r"a{2}[ab]{2}\s{2}", parser=self.parser)
        expected = set("aa" + c * 2 + s * 2 for c in "ab" for s in SYMBOLS)

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate(r"a{0,1}[ab]{0,1}\s{0,1}", parser=self.parser)
        expected = set(
            "a" * m1 + c * m2 + s * m3
            for c in "ab"
            for s in SYMBOLS
            for m1 in range(0, 2)
            for m2 in range(0, 2)
            for m3 in range(0, 2)
        ) - {""}

        self.assertSetEqual(results, expected)


class TestEdgeCases(BaseTest):
    def test_empty(self):
        results = generate(r"a{0}", parser=self.parser)

        self.assertSetEqual(results, set())


class TestNonCaptureGroup(BaseTest):
    def test_simple(self):
        results = generate(r"(:asd)", parser=self.parser)

        self.assertSetEqual(results, {"asd"})

    def test_double(self):
        results = generate(r"(:asd)(:dsa)", parser=self.parser)

        self.assertSetEqual(results, {"asddsa"})

    def test_composite(self):
        results = generate(r"(:a[ab])\s", parser=self.parser)
        expected = set("a" + c + s for c in "ab" for s in SYMBOLS)

        self.assertSetEqual(results, expected)

    def test_repetition(self):
        results = generate(r"(:ab){2}", parser=self.parser)
        expected = {"abab"}

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate(r"(:ab){1,2}", parser=self.parser)
        expected = {"abab", "ab"}

        self.assertSetEqual(results, expected)

    def test_variable_repetition(self):
        results = generate(r"(:[ab]){2}", parser=self.parser)
        expected = {"aa", "bb"}

        self.assertSetEqual(results, expected)

    def test_variable_minmax(self):
        results = generate(r"(:[ab]){1,2}", parser=self.parser)
        expected = {"a", "b", "aa", "bb"}

        self.assertSetEqual(results, expected)

    def test_variable_pattern_repetition(self):
        results = generate(r"(:b[ae])!{2}", parser=self.parser)
        expected = {"baba", "babe", "beba", "bebe"}

        self.assertSetEqual(results, expected)

    def test_variable_pattern_minmax(self):
        results = generate(r"(:b[ae])!{1,2}", parser=self.parser)
        expected = {"ba", "be", "baba", "babe", "beba", "bebe"}

        self.assertSetEqual(results, expected)

    def test_nested_simple(self):
        results = generate(r"(:a(:bc)d)", parser=self.parser)

        self.assertSetEqual(results, {"abcd"})

    def test_nested_repetition_inside(self):
        results = generate(r"(:a(:bc){2}d)", parser=self.parser)

        self.assertSetEqual(results, {"abcbcd"})

    def test_nested_repetition_outside(self):
        results = generate(r"(:a(:bc)d){2}", parser=self.parser)

        self.assertSetEqual(results, {"abcdabcd"})

    def test_nested_repetition_all(self):
        results = generate(r"(:a(:bc){2}d){2}", parser=self.parser)

        self.assertSetEqual(results, {"abcbcdabcbcd"})


class TestCaptureGroup(BaseTest):
    def test_simple(self):
        results = generate(r"(asd)", parser=self.parser)

        self.assertSetEqual(results, {"asd"})

    def test_double(self):
        results = generate(r"(asd)(dsa)", parser=self.parser)

        self.assertSetEqual(results, {"asddsa"})

    def test_composite(self):
        results = generate(r"(a[ab])\s", parser=self.parser)
        expected = set("a" + c + s for c in "ab" for s in SYMBOLS)

        self.assertSetEqual(results, expected)

    def test_repetition(self):
        results = generate(r"(ab){2}", parser=self.parser)
        expected = {"abab"}

        self.assertSetEqual(results, expected)

    def test_composite_repetition(self):
        results = generate(r"(a[ab]){3}", parser=self.parser)
        expected = {"aaaaaa", "ababab"}

        self.assertSetEqual(results, expected)

    def test_minmax(self):
        results = generate(r"(ab){1,2}", parser=self.parser)
        expected = {"abab", "ab"}

        self.assertSetEqual(results, expected)

    def test_nested_simple(self):
        results = generate(r"(a(bc)d)", parser=self.parser)

        self.assertSetEqual(results, {"abcd"})

    def test_nested_repetition_inside(self):
        results = generate(r"(a(bc){2}d)", parser=self.parser)

        self.assertSetEqual(results, {"abcbcd"})

    def test_nested_repetition_outside(self):
        results = generate(r"(a(bc)d){2}", parser=self.parser)

        self.assertSetEqual(results, {"abcdabcd"})

    def test_nested_repetition_all(self):
        results = generate(r"(a(bc){2}d){2}", parser=self.parser)

        self.assertSetEqual(results, {"abcbcdabcbcd"})


class TestAlternativesGroup(BaseTest):
    def test_simple(self):
        results = generate(r"(asd|lol)", parser=self.parser)

        self.assertSetEqual(results, {"asd", "lol"})

    def test_non_capture(self):
        results = generate(r"(:asd|lol)", parser=self.parser)

        self.assertSetEqual(results, {"asd", "lol"})

    def test_many(self):
        results = generate(r"(asd|lol|lmao|kek)", parser=self.parser)

        self.assertSetEqual(results, {"asd", "lol", "lmao", "kek"})

    def test_with_variation(self):
        results = generate(r"(asd|lol[ae])", parser=self.parser)

        self.assertSetEqual(results, {"asd", "lola", "lole"})

    def test_with_internal_repetition(self):
        results = generate(r"(asd{2}|lol{3})", parser=self.parser)

        self.assertSetEqual(results, {"asdd", "lolll"})

    def test_with_internal_minmax(self):
        results = generate(r"(asd{1,2}|lol{1,3})", parser=self.parser)

        self.assertSetEqual(results, {"asd", "asdd", "lol", "loll", "lolll"})

    def test_repetition(self):
        results = generate(r"(asd|lol){2}", parser=self.parser)

        self.assertSetEqual(results, {"asdasd", "lollol"})

    def test_minmax(self):
        results = generate(r"(asd|lol){1,2}", parser=self.parser)

        self.assertSetEqual(results, {"asd", "asdasd", "lol", "lollol"})

    def test_pattern_repetition(self):
        results = generate(r"(asd|lol)!{2}", parser=self.parser)

        self.assertSetEqual(results, {"asdasd", "lollol", "asdlol", "lolasd"})

    def test_pattern_minmax(self):
        results = generate(r"(asd|lol)!{1,2}", parser=self.parser)

        self.assertSetEqual(
            results, {"asdasd", "lollol", "asdlol", "lolasd", "asd", "lol"}
        )


class TestGroupReferences(BaseTest):
    @unittest.skip("Not implemented")
    def test_simple_reference(self):
        results = generate(r"(asd)a\0", parser=self.parser)

        self.assertSetEqual(results, {"asdaasd"})

    @unittest.skip("Not implemented")
    def test_composite_reference(self):
        results = generate(r"(a[ab])\0", parser=self.parser)
        expected = {"aaaa", "abab"}

        print(results)
        self.assertSetEqual(results, expected)

    @unittest.skip("Not implemented")
    def test_double_reference(self):
        results = generate(r"(a(b))\1\0", parser=self.parser)

        self.assertSetEqual(results, {"ababb"})

    @unittest.skip("Not implemented")
    def test_nested_reference(self):
        results = generate(r"(a(b)\0)\1", parser=self.parser)

        self.assertSetEqual(results, {"abbabb"})

    @unittest.skip("Not implemented")
    def test_multiple_reference(self):
        results = generate(r"(asd)\0\0", parser=self.parser)

        self.assertSetEqual(results, {"asdasdasd"})

    @unittest.skip("Not implemented")
    def test_multiple_composite_reference(self):
        results = generate(r"([ae]sd)\0\0", parser=self.parser)

        self.assertSetEqual(results, {"asdasdasd", "esdesdesd"})

    @unittest.skip("Not implemented")
    def test_composite_reference_repetition(self):
        results = generate(r"([ae]sd){2}", parser=self.parser)

        self.assertSetEqual(results, {"asdasd", "esdesd"})

    @unittest.skip("Not implemented")
    def test_composite_reference_minmax(self):
        results = generate(r"([ae]sd){1,2}", parser=self.parser)

        self.assertSetEqual(results, {"asd", "esd", "asdasd", "esdesd"})

    def test_pattern_reference(self):
        results = generate(r"(a[ab])\0!", parser=self.parser)
        expected = {"aaaa", "abab", "abaa", "aaab"}

        print(results)
        self.assertSetEqual(results, expected)

    def test_pattern_reference_repetition(self):
        results = generate(r"(a[ab])v\0!{2}", parser=self.parser)
        expected = {
            "aavaaaa",
            "aavaaab",
            "aavabaa",
            "aavabab",
            "abvaaaa",
            "abvabaa",
            "abvaaab",
            "abvabab",
        }

        print(results)
        self.assertSetEqual(results, expected)

    def test_pattern_reference_minmax(self):
        results = generate(r"(a[ab])v\0!{1,2}", parser=self.parser)
        expected = {
            "aavaaaa",
            "aavaaab",
            "aavabaa",
            "aavabab",
            "abvaaaa",
            "abvabaa",
            "abvaaab",
            "abvabab",
            "aavaa",
            "aavab",
            "abvaa",
            "abvab",
        }

        print(results)
        self.assertSetEqual(results, expected)

    @unittest.skip("Not implemented")
    def test_options_reference(self):
        results = generate(r"(asd|lol)u\0", parser=self.parser)

        self.assertSetEqual(results, {"asduasd", "lolulol"})

    @unittest.skip("Not implemented")
    def test_options_reference_repetition(self):
        results = generate(r"(asd|lol)u\0{2}", parser=self.parser)

        self.assertSetEqual(results, {"asduasdasd", "lolulollol"})

    @unittest.skip("Not implemented")
    def test_options_reference_minmax(self):
        results = generate(r"(asd|lol)u\0{0,2}", parser=self.parser)

        self.assertSetEqual(results, {
            "asdu",
            "lolu",
            "asduasd",
            "lolulol",
            "asduasdasd",
            "lolulollol",
        })

    def test_options_pattern_reference(self):
        results = generate(r"(asd|lol)u\0!", parser=self.parser)

        self.assertSetEqual(results, {"asduasd", "lolulol", "asdulol", "loluasd"})

    def test_options_pattern_reference_repetition(self):
        results = generate(r"(asd|lol)u\0!{2}", parser=self.parser)

        self.assertSetEqual(
            results,
            {
                "asduasdasd",
                "asduasdlol",
                "asdulolasd",
                "asdulollol",
                "loluasdasd",
                "loluasdlol",
                "lolulolasd",
                "lolulollol",
            },
        )

    def test_options_pattern_reference_minmax(self):
        results = generate(r"(asd|lol)u\0!{1,2}", parser=self.parser)

        self.assertSetEqual(
            results,
            {
                "asduasd",
                "asdulol",
                "loluasd",
                "lolulol",
                "asduasdasd",
                "asduasdlol",
                "asdulolasd",
                "asdulollol",
                "loluasdasd",
                "loluasdlol",
                "lolulolasd",
                "lolulollol",
            },
        )


class TestErrors(BaseTest):
    def test_nonexistent_meta_sequence(self):
        self.assertRaises(NoMatch, generate, r"\y", parser=self.parser)

    @unittest.skip("Not implemented")
    def test_reference_before(self):
        self.assertRaises(
            GroupNotFoundException, generate, r"\0(asd)", parser=self.parser
        )

    @unittest.skip("Not implemented")
    def test_recursive_reference(self):
        self.assertRaises(
            GroupNotFoundException, generate, r"(asd(lol\0)\0)\0", parser=self.parser
        )

    @unittest.skip("Not implemented")
    def test_nonexistent_group(self):
        self.assertRaises(
            GroupNotFoundException, generate, r"asd\1", parser=self.parser
        )

    def test_nonsensical_quantifier(self):
        self.assertRaises(
            NonsensicalQuantifierException, generate, r"asd{3,1}", parser=self.parser
        )

    def test_unclosed_char_class(self):
        self.assertRaises(NoMatch, generate, r"a[sd", parser=self.parser)

    def test_unclosed_group(self):
        self.assertRaises(NoMatch, generate, r"a(sd", parser=self.parser)

    def test_unopened_char_class(self):
        self.assertRaises(NoMatch, generate, r"as]d", parser=self.parser)

    def test_unopened_group(self):
        self.assertRaises(NoMatch, generate, r"as)d", parser=self.parser)

    def test_multiple_negative_char_class_markers(self):
        self.assertRaises(NoMatch, generate, r"[^^asd]", parser=self.parser)

    def test_multiple_non_capture_group_markers(self):
        self.assertRaises(NoMatch, generate, r"(::asd)", parser=self.parser)

    def test_multiple_choice_markers(self):
        self.assertRaises(NoMatch, generate, r"(a||sd)", parser=self.parser)


class TestWarnings(BaseTest):
    def test_empty_input(self):
        self.assertWarns(SyntaxWarning, generate, r"", parser=self.parser)

    def test_one_char_char_class(self):
        self.assertWarns(SyntaxWarning, generate, r"[a]", parser=self.parser)

    def test_zero_quantifier_literal(self):
        self.assertWarns(SyntaxWarning, generate, r"a{0}", parser=self.parser)

    def test_one_quantifier_literal(self):
        self.assertWarns(SyntaxWarning, generate, r"a{1}", parser=self.parser)

    def test_one_quantifier_minmax(self):
        self.assertWarns(SyntaxWarning, generate, r"a{1,1}", parser=self.parser)

    def test_zero_quantifier_char_class(self):
        self.assertWarns(SyntaxWarning, generate, r"[asd]{0}", parser=self.parser)

    def test_zero_quantifier_group(self):
        self.assertWarns(SyntaxWarning, generate, r"(asd){0}", parser=self.parser)

    def test_zero_quantifier_minmax(self):
        self.assertWarns(SyntaxWarning, generate, r"a{0,0}", parser=self.parser)

    def test_quantifier_same_minmax(self):
        self.assertWarns(SyntaxWarning, generate, r"a{3,3}", parser=self.parser)
