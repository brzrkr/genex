# Genex
Genex allows you to parse Genex expressions and produce outputs from them. Check out `Grammar.md` for an overview
of the Genex grammar.

## Usage
Genex can be used in the following ways, all of which require a Python 3.9 interpreter
or higher:
- As a Python module.
- Interactively in the command line.
- With a direct input.
- With a file input.

Note that in any of the methods, each expression is completely separate from the others,
so the values of capture groups will not persist from one to the other.

### As a Python module
1. `import genex.generate`.
2. feed your input string to `genex.generate()`, which will return a set of all outputs.

### Interactively in the command line
1. Run `genex.py` with your Python interpreter.
2. Enter a Genex expression
3. Genex will print the generated outputs separated by newlines and wait for more input.
4. To exit, send an EOF or input `exit`.

### With a direct input
1. Run `genex.py INPUT`, where `INPUT` is a Genex expression.
2. Genex will print all generated outputs.

### With a file input
1. Prepare an input file with a Genex expression per line.
2. Run `genex.py -f FILE_PATH`, where `FILE_PATH` is the path to the input file.
    - Instead of `-f`, `--file` or `-` are also allowed.
3. Genex will print all generated outputs separated by newlines.
	Each group of outputs, one per expression in the input file, will be separated by an
	empty line (an extra newline).

## Known issues / unimplemented features
- Group references (`\0`, `\1`...) currently do not work, but the output they would give
is included in the outputs of pattern references (`\0!`, `\1!`...).
