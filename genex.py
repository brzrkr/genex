from typing import Set
from arpeggio import NoMatch, ParserPython, visit_parse_tree
from rules import genex_root
from analyzer import GenexAnalyzer
import sys
from warnings import warn as warn_


def warn(message: str):
    warn_(message, SyntaxWarning)


# TODO option to silence warnings

PARSER_CONFIG = {
    "ws": "\t\r\n",
    "skipws": True,
    "memoization": True,
}

genex_parser = ParserPython(genex_root, **PARSER_CONFIG)


def generate(input_string: str, parser: ParserPython = genex_parser) -> Set[str]:
    if not input_string:
        warn("Empty expression supplied.")

        return set()

    parse_tree = parser.parse(input_string)

    return visit_parse_tree(parse_tree, GenexAnalyzer())


def print_outputs(outputs: set[str]):
    message = "\n".join(outputs) or "No output produced."
    print(message)


class MatchingError(Exception):
    def __init__(self, expression: str, exception: NoMatch):
        self.rules = exception.rules
        self.position = exception.parser.pos_to_linecol(exception.position)
        self.parser = exception.parser
        self.expression = expression


def interactive_mode():
    try:
        while user_input := input("Enter a genex or `exit`:\n"):
            try:
                if user_input.lower() == "exit":
                    raise EOFError

                print_outputs(generate(user_input))
            except NoMatch as match_exc:
                raise MatchingError(user_input, match_exc)

    except EOFError:
        exit(0)


def file_mode():
    with open(sys.argv[2], "r").read() as user_input:
        inputs = user_input.split("\n")

        if not inputs:
            print_outputs(set())
            exit(0)

        for idx, expression in enumerate(inputs):
            try:
                print_outputs(generate(expression))

                if idx < len(inputs) - 1:
                    print()
            except NoMatch as match_exc:
                raise MatchingError(expression, match_exc)


def direct_mode():
    user_input = " ".join(sys.argv[1:])

    try:
        print_outputs(generate(user_input))
    except NoMatch as match_exc:
        raise MatchingError(user_input, match_exc)


if __name__ == "__main__":
    try:
        if (n_args := len(sys.argv)) == 1:
            interactive_mode()
        elif n_args == 3 and sys.argv[1].lower() in ("-", "-f", "--file"):
            file_mode()
        else:
            direct_mode()

    except MatchingError as e:
        rules = ",".join(map(lambda r: r.name, e.rules))
        print(
            f"Error at line {e.position[0]}, column {e.position[1]} while parsing the expression:\n"
            f"{e.expression}\n"
            f"Rules tried:\n{rules}"
        )
        exit(1)
