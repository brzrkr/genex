class GroupNotFoundException(Exception):
    pass


class NonsensicalQuantifierException(Exception):
    pass
