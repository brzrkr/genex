# Genex Grammar
Genex (generative expression) is a mini-language inspired by regular expressions for the 
generation of strings based on user-defined patterns.

**NOTE:** content references for capture groups are not implemented at the moment. Only
pattern references work.

# Definitions and meta sequences
1. Only letters, apostrophes, hyphens or spaces are produced. Together, these are 
referred to as *word characters*. *Note that this definition is different from 
regex's.*
1. The following meta sequences are defined:
    - `\s` produces any of `'`, `-` or a space (each a *symbol*).
    - `\S` produces any word character that is **not** a symbol (a *letter*).
    - `\v` produces any of `e`, `u`, `i`, `o`, `a`, `ö`, `ü` and uppercase variants 
    (each a *vowel*).
    - `\V` produces any word character that is **not** a vowel.
    - `\c` produces any of `q`, `w`, `r`, `t`, `y`, `p`, `s`, `d`, `f`, `g`, `h`, `j`, 
    `k`, `l`, `z`, `x`, `c`, `v`, `b`, `n`, `m` and uppercase variants 
    (each a *consonant*).
    - `\C` produces any word character that is **not** a consonant.
    - `\u` produces any letter that is uppercase.
    - `\U` produces any letter that is lowercase.
    - `\w` produces any word character (meaning any of the afore-mentioned characters).

# Syntax
1. Any word character at the base level is interpreted literally:
    - `Sad` produces `Sad`.
1. Two or more symbols can be enclosed in square brackets `[ ]` (called a 
*character class*) to indicate that any such character is acceptable in that position.  
    - `S[ao]d` produces `Sad` and `Sod`.
1. Two or more symbols can be enclosed in a character class with a caret `[^ ]` (called 
a *negative character class*) to indicate that any symbol that is **not** included is 
acceptable in that position:
    - `S[^\so]` produces any two-letter word starting with S and ending with anything 
    but a symbol or an `o`, including, but not limited to, `Sv`, `Sa`, `Su`.
1. Any word character, meta sequence, or character class can be repeated any number of 
times by indicating a minimum number, and optionally a maximum number after a comma, 
within curly brackets `{ }`. This is called a *repetition clause*.
Prepending a `!` to the repetition clause before a meta sequence or character class will 
cause it to repeat the pattern instead of the produced result (see "group" below).
    - `S{2}` produces `SS`.
    - `S{1,2}a{0,1}` produces `S`, `Sa`, `SS`, `SSa`.
    - `[fa]{2}` produces `ff`, `aa`.
    - `[fa]!{2}` produces `ff`, `fa`, `af`, `aa`.
1. Any valid pattern can be enclosed in parentheses `( )` to form a *group*. Groups can 
be used to apply a repetition clause to an entire pattern section or to replicate it 
elsewhere.  
Appending a `!` to a group before a repetition will cause it to repeat the pattern itself 
rather than the produced output:
    - `(S{0,1}a){1,2}` produces `a`, `aa`, `Sa`, `SaSa`.
    - `(S{0,1}a)!{1,2}` produces `aa`, `aSa`, `SaSa`, `Saa`.
1. A group can be referenced using the meta character `\` followed by a number from 0 
to 9 to reference the corresponding group. This means that **only 10 groups are supported.**  
Here too, appending a `!` to a *group reference* will cause it to replicate the pattern 
enclosed in the group instead of the produced output.
    - `(S{0,1}a)e\0` produces `aea`, `SaeSa`.
    - `(S{0,1}a)e\0!` produces `aea`, `aeSa`, `SaeSa`, `Saea`.
1. A group can begin with `:` to make it a *non-capture group*. Non-capture groups behave 
like normal groups but cannot be referenced, and thus do not count for the maximum number 
of groups.
    - `(:Sa)(fa)\0` produces `Safafa`.
1. Groups are referenced in the order of their *closing*. For example:
    - `(S(a)f\0)\1` produces `SafaSafa`.
    - `(S(a)f\1)\0` is an error, because when `\1` is reached, only one group (indexed 0)
    has been closed, and no group indexed 1 can be referenced yet.
1. Groups can be groups of alternatives, or *choice groups* by separating options with `|`. This will output
one of the options contained inside the group, and works with both capture and non-capture
groups. Example:
    - `(Sa|fa|e)` produces `Sa`, `fa`, `e`.

## Warnings
There are several possibilities for essentially useless or redundant instructions such 
as duplicate characters in character classes `[ssa]`, character classes that produce no 
character `[^\w]`, repetition clauses that amount to zero `sa{0}` or with redundant 
values `S{2,2}` and so on.  
Such cases produce `UserWarning`s, but they do not interrupt the program. It is 
advised to remove any source of warnings to improve performance and pattern readability.
