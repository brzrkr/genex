import re

from arpeggio import OneOrMore, EOF, Optional, ZeroOrMore
from arpeggio import RegExMatch as Regex

# TODO configurable base sets
SYMBOLS = frozenset("-' ")
VOWELS = frozenset("aeiouöüAEIOUÖÜ")
CONSONANTS = frozenset("qwrtypsdfghjklzxcvbnmQWRTYPSDFGHJKLZXCVBNM")
UPPERCASE = frozenset("AEIOUÖÜQWRTYPSDFGHJKLZXCVBNM")
LOWERCASE = frozenset("aeiouöüqwrtypsdfghjklzxcvbnm")
WORD_CHARS = VOWELS | CONSONANTS | SYMBOLS
NON_CONSONANTS = WORD_CHARS - CONSONANTS
NON_VOWELS = WORD_CHARS - VOWELS
LETTERS = WORD_CHARS - SYMBOLS

META_SETS: dict[str, frozenset[str]] = {
    "s": SYMBOLS,
    "S": LETTERS,
    "v": VOWELS,
    "V": NON_VOWELS,
    "c": CONSONANTS,
    "C": NON_CONSONANTS,
    "u": UPPERCASE,
    "U": LOWERCASE,
    "w": WORD_CHARS,
}


def number():
    # language=RegExp
    return Regex(r"[0-9]")


def pattern_marker():
    return Regex("!")


def raw_quantifier():
    return "{", number, Optional(",", number), "}"


def pattern_quantifier():
    return pattern_marker, raw_quantifier


def quantifier():
    return [pattern_quantifier, raw_quantifier]


def unquantified_meta_sequence():
    return (
        "\\",
        Regex(rf"[{''.join(META_SETS.keys())}]"),
    )


def meta_sequence():
    return unquantified_meta_sequence, Optional([quantifier, pattern_marker])


def group_reference():
    return (
        "\\",
        number,
        Optional(pattern_marker),
        Optional(raw_quantifier),
    )


def char():
    # language=RegExp
    return Regex(r"[a-zA-Z-' ]", re_flags=re.UNICODE)


def literal():
    return [(char, raw_quantifier), char]


def negative_class_marker():
    # language=RegExp
    return Regex(r"\^")


def character_class():
    return (
        "[",
        Optional(negative_class_marker),
        OneOrMore([unquantified_meta_sequence, char]),
        "]",
        Optional(quantifier),
    )


def non_capture_marker():
    # language=RegExp
    return Regex(r":")


def choice_separator():
    # language=RegExp
    return Regex(r"\|")


def group():
    return (
        "(",
        Optional(non_capture_marker),
        OneOrMore(expression),
        ZeroOrMore((choice_separator, OneOrMore(expression))),
        ")",
        Optional(quantifier),
    )


def expression():
    return [group, character_class, meta_sequence, group_reference, literal]


def genex_root():
    return OneOrMore(expression), EOF
