from itertools import chain, groupby, product
from typing import Generator, Iterator, Literal, Optional, Union, TypeVar
from arpeggio import SemanticActionResults, PTNodeVisitor, NonTerminal, Terminal
from exceptions import (
    GroupNotFoundException,
    NonsensicalQuantifierException,
)
from rules import META_SETS
from collections.abc import Iterable
from warnings import warn as warn_


def warn(message: str):
    warn_(message, SyntaxWarning)


class Quantifier(Iterable):
    """Defines minimum and maximum repetition times for a quantifiable container."""

    __slots__ = ["min_", "max_", "is_pattern"]

    def __init__(
        self, min_: int, max_: Optional[int] = None, *, is_pattern: bool = False
    ):
        self.min_ = min_
        self.max_ = max_ or min_

        if min_ > self.max_:
            raise NonsensicalQuantifierException(
                "Value for max must be more than or equal to min."
            )
        elif not max_:
            if min_ == 0:
                warn("Quantifier with 0 as only result can be removed with its item.")
            elif min_ == 1:
                warn("Quantifier with 1 as only result can be removed.")
        elif min_ == max_:
            warn("Quantifier with same minimum and maximum can be simplified.")

        self.is_pattern = is_pattern

    def __iter__(self) -> Iterator[int]:
        return iter((self.min_, self.max_))

    def __repr__(self):
        return f"{'!' if self.is_pattern else ''}{{{self.min_},{self.max_}}}"


class Group(Iterable):
    """Contains expressions, including other groups."""

    __slots__ = ["options", "quantifier", "is_capture"]

    def __init__(
        self,
        options: Optional[list[list]] = None,
        quantifier: Optional[Quantifier] = None,
        *,
        is_capture: bool = True,
    ):
        # (asd|lol[ae]) == [[['asd']], [['lol'], ["a", "e"]]]
        self.options = [option for option in options or ()]
        self.quantifier = quantifier
        self.is_capture = is_capture

    def __repr__(self):
        return (
            f"({'' if self.is_capture else ':'}{self.options}){self.quantifier or ''}"
        )

    def __iter__(self) -> Iterator[str]:
        computed_options = []
        for option in self.options:
            computed_options.append(
                [
                    "".join(output)
                    for output in product(
                        *[["".join(combination) for combination in product(*option)]]
                    )
                ]
            )

        if not (quantifier := self.quantifier):
            return chain(*computed_options)

        if not quantifier.is_pattern:
            return chain(
                quantified
                for option in computed_options
                for quantified in quantify(option, self.quantifier)
            )

        items = [item for generator in computed_options for item in generator]

        out = []
        for times in range(quantifier.min_, quantifier.max_ + 1):
            combined_options = [
                option for option in mix_string_pieces(items, times=times)
            ]
            out.append(combined_options)

        return chain(*out)


class GroupReference:
    """Refers to an existing capture group."""

    __slots__ = ["symbol", "quantifier", "is_pattern", "group"]

    def __init__(self, symbol: int, is_pattern: bool, quantifier: Optional[Quantifier]):
        self.symbol = symbol
        self.is_pattern = is_pattern
        self.quantifier = quantifier
        self.group: Optional[Group] = None

    def __repr__(self):
        return rf"\{self.symbol}{'' if self.is_pattern else ''}{self.quantifier or ''}"

    def _unpack_pattern_ref(self) -> Iterator[str]:
        if not (quantifier := self.quantifier):
            return iter(self.group)

        out = []
        for times in range(quantifier.min_, quantifier.max_ + 1):
            out.append(mix_string_pieces(self.group, times))

        return chain(*out)

    def __iter__(self) -> Iterator[str]:
        if self.group is None:
            raise GroupNotFoundException

        if not self.is_pattern:
            raise NotImplementedError

        return self._unpack_pattern_ref()


class CharClass(Iterable):
    """Contains individual characters. When iterated, produces all possible outputs."""

    __slots__ = ["chars", "quantifier"]

    def __init__(
        self,
        chars: Optional[set[str]] = None,
        quantifier: Optional[Quantifier] = None,
    ):
        self.chars = chars or set()
        self.quantifier = quantifier

    def __repr__(self):
        return f"[{''.join(self.chars)}]{self.quantifier or ''}"

    def __iter__(self) -> Iterator[str]:
        if (quantifier := self.quantifier) :
            if quantifier.is_pattern:
                out = []
                for times in range(quantifier.min_, quantifier.max_ + 1):
                    out.append(mix_string_pieces(self.chars, times))

                return chain(*out)

            return iter(quantify(self.chars, quantifier))

        return iter(self.chars)


UnaryMarker = Literal[True]
ExpressionContents = Union[CharClass, Group, GroupReference, list[str]]
T = TypeVar("T")


def quantify(contents: Iterable[T], quantity: Quantifier) -> Generator[T, None, None]:
    min_, max_ = quantity

    return (content * times for times in range(min_, max_ + 1) for content in contents)


def mix_string_pieces(pieces: Iterable[str], times: int = 1) -> Iterator[str]:
    return ("".join(text) for text in product(pieces, repeat=times))


class GenexAnalyzer(PTNodeVisitor):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.captured_groups: list[Group] = []
        self.captured_outputs: list[list[str]] = []
        """Collection of capture group outputs indexed the same way as group references.
        
        Each contained list is a collection of possible outputs of the group.
        """

    def visit_number(self, node: Terminal, _: SemanticActionResults) -> int:
        return int(node.value)

    def visit_raw_quantifier(
        self,
        _: NonTerminal,
        children: SemanticActionResults[int],
    ) -> Quantifier:
        min_ = children[0]
        max_ = None

        if len(children) == 2:
            max_ = children[1]

        return Quantifier(min_, max_)

    def visit_pattern_quantifier(
        self,
        _: NonTerminal,
        children: SemanticActionResults[Quantifier],
    ) -> Quantifier:
        quantifier = children[1]
        quantifier.is_pattern = True

        return quantifier

    def visit_char(self, node: Terminal, _: SemanticActionResults) -> str:
        return node.value

    def visit_unquantified_meta_sequence(
        self,
        _: Terminal,
        children: SemanticActionResults[str],
    ) -> CharClass:
        meta_symbol: str = children[0]

        return CharClass(set(META_SETS[meta_symbol]))

    def visit_meta_sequence(
        self,
        _: NonTerminal,
        children: SemanticActionResults[Union[CharClass, Quantifier]],
    ) -> CharClass:
        sequence: CharClass = children.unquantified_meta_sequence[0]

        if children.quantifier:
            sequence.quantifier = children.quantifier[0]

        return sequence

    def visit_group_reference(
        self,
        _: NonTerminal,
        children: SemanticActionResults[Union[int, UnaryMarker, Quantifier]],
    ) -> GroupReference:
        group_index: int = children[0]
        is_pattern = bool(children.pattern_marker)
        quantifier = children.raw_quantifier[0] if children.raw_quantifier else None

        return GroupReference(group_index, is_pattern, quantifier)

    def visit_literal(
        self,
        _: NonTerminal,
        children: SemanticActionResults[Union[str, Quantifier]],
    ) -> list[str]:
        char = children[0]

        if len(children) == 1:
            # Just char
            return [char]
        else:
            # With quantifier
            quantified = list(quantify(char, children[1]))
            return quantified

    def visit_negative_class_marker(
        self,
        _: Terminal,
        __: SemanticActionResults,
    ) -> UnaryMarker:
        return True

    def visit_character_class(
        self,
        _: NonTerminal,
        children: SemanticActionResults[Union[UnaryMarker, str, CharClass, Quantifier]],
    ) -> CharClass:
        elements = set(children.char)

        if len(elements) == 1:
            warn("Character class containing only one item can be turned into literal.")

        for meta_set in children.unquantified_meta_sequence:
            meta_set: CharClass
            elements.update(meta_set.chars)

        if children.negative_class_marker:
            elements ^= META_SETS["w"]

        return CharClass(
            elements, quantifier=children.quantifier[0] if children.quantifier else None
        )

    def visit_non_capture_marker(
        self,
        _: Terminal,
        __: SemanticActionResults,
    ) -> UnaryMarker:
        return True

    def visit_group(
        self,
        _: NonTerminal,
        children: SemanticActionResults[
            Union[UnaryMarker, ExpressionContents, Quantifier]
        ],
    ) -> Group:
        is_capture = not bool(children.non_capture_marker)
        options = [
            list(v)
            for k, v in groupby(
                filter(lambda c: not isinstance(c, (bool, Quantifier)), children),
                key=lambda x: x == "|",
            )
            if not k
        ]

        output = Group(
            options,
            quantifier=None,
            is_capture=is_capture,
        )

        if is_capture:
            self.captured_groups.append(output)

        output.quantifier = children.quantifier[0] if children.quantifier else None

        return output

    def visit_expression(
        self,
        _: NonTerminal,
        children: SemanticActionResults[ExpressionContents],
    ) -> ExpressionContents:
        return children[0]

    def visit_genex_root(
        self,
        _: NonTerminal,
        children: SemanticActionResults[ExpressionContents],
    ) -> set[str]:
        def make_group_ref(child):
            if not isinstance(child, GroupReference):
                return child

            try:
                child.group = self.captured_groups[child.symbol]
            except IndexError:
                raise GroupNotFoundException

            return child

        elements = list(
            map(
                make_group_ref,
                children,
            )
        )

        # >>> ["".join(output) for output in i.product(*[["lol"], ["a", "i"]])]
        # ['lola', 'loli']
        contents = set("".join(output) for output in product(*elements)) - {""}

        if not contents:
            warn("Expression produces no output.")

        return contents
